
AUTO : bin/tarea2 
OBJ_DIR = ./obj
INCL_DIR = -Ilib 

$(OBJ_DIR)/tarea2.o: app/tarea2.c lib/numerotes.h lib/multiplicacion.h lib/reduccion.h lib/division.h lib/exponenciacion.h lib/euclides.h lib/nist.h
	$(CC) $(PROF) $(FLAGS_CC) -o $(OBJ_DIR)/tarea2.o $(INCL_DIR) -c app/tarea2.c 
	$(COMMANDS) 

$(OBJ_DIR)/numerotes.o: src/numerotes.c lib/numerotes.h
	$(CC) $(PROF) $(FLAGS_CC) -o $(OBJ_DIR)/numerotes.o $(INCL_DIR) -c src/numerotes.c 
	$(COMMANDS) 

$(OBJ_DIR)/euclides.o: src/euclides.c lib/euclides.h lib/reduccion.h lib/numerotes.h
	$(CC) $(PROF) $(FLAGS_CC) -o $(OBJ_DIR)/euclides.o $(INCL_DIR) -c src/euclides.c 
	$(COMMANDS) 

$(OBJ_DIR)/exponenciacion.o: src/exponenciacion.c lib/exponenciacion.h lib/reduccion.h lib/numerotes.h
	$(CC) $(PROF) $(FLAGS_CC) -o $(OBJ_DIR)/exponenciacion.o $(INCL_DIR) -c src/exponenciacion.c 
	$(COMMANDS) 

$(OBJ_DIR)/reduccion.o: src/reduccion.c lib/reduccion.h lib/numerotes.h
	$(CC) $(PROF) $(FLAGS_CC) -o $(OBJ_DIR)/reduccion.o $(INCL_DIR) -c src/reduccion.c 
	$(COMMANDS) 

$(OBJ_DIR)/division.o: src/division.c lib/division.h lib/reduccion.h lib/numerotes.h
	$(CC) $(PROF) $(FLAGS_CC) -o $(OBJ_DIR)/division.o $(INCL_DIR) -c src/division.c 
	$(COMMANDS) 

$(OBJ_DIR)/nist.o: src/nist.c lib/nist.h lib/numerotes.h
	$(CC) $(PROF) $(FLAGS_CC) -o $(OBJ_DIR)/nist.o $(INCL_DIR) -c src/nist.c 
	$(COMMANDS) 

$(OBJ_DIR)/misc.o: src/misc.c lib/misc.h
	$(CC) $(PROF) $(FLAGS_CC) -o $(OBJ_DIR)/misc.o $(INCL_DIR) -c src/misc.c 
	$(COMMANDS) 

$(OBJ_DIR)/multiplicacion.o: src/multiplicacion.c lib/multiplicacion.h lib/reduccion.h lib/numerotes.h
	$(CC) $(PROF) $(FLAGS_CC) -o $(OBJ_DIR)/multiplicacion.o $(INCL_DIR) -c src/multiplicacion.c 
	$(COMMANDS) 

ALL_OBJ = $(OBJ_DIR)/tarea2.o $(OBJ_DIR)/numerotes.o $(OBJ_DIR)/euclides.o $(OBJ_DIR)/exponenciacion.o $(OBJ_DIR)/reduccion.o $(OBJ_DIR)/division.o $(OBJ_DIR)/nist.o $(OBJ_DIR)/misc.o $(OBJ_DIR)/multiplicacion.o 

bin/tarea2 : $(ALL_OBJ)
	cc -o bin/tarea2 $(ALL_OBJ)
