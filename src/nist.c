#include"nist.h"

/***********************************************/
numero p192(numerote R, numerote C)
	{
	numerote p192 = genN();
	numerote e192 = genN();
	numerote e64 = genN();
	numerote e0 = genN();
	numerote s1 = genN();
	numerote s2 = genN();
	numerote s3 = genN();
	numerote s4 = genN();
	
	exp2N(e192,192);
	exp2N(e64,64);
	exp2N(e0,0);
	resN(p192,e192,e64);
	resN(p192,p192,e0);
	
	printf("p192:=%s;\n",txtN(p192));
	
	s1[0] = C[0];
	s1[1] = C[1];
	s1[2] = C[2];
	
	s2[0] = C[3];
	s2[1] = C[3];
	s2[2] = 0;
	
	s3[0] = 0;
	s3[1] = C[4];
	s3[2] = C[4];
	
	s4[0] = C[5];
	s4[1] = C[5];
	s4[2] = C[5];
	
	sumN(R,s1,s2);
	if(cmpN(R,p192)>=0)
		{
		resN(R,R,p192);
		}
	sumN(R,R,s3);
	if(cmpN(R,p192)>=0)
		{
		resN(R,R,p192);
		}
	sumN(R,R,s4);
	if(cmpN(R,p192)>=0)
		{
		resN(R,R,p192);
		}
	return(0);
	}
/***********************************************/

/***********************************************/
numero p224(numerote R, numerote C)
	{
	numerote p224 = genN();
	numerote e224 = genN();
	numerote e96 = genN();
	numerote e0 = genN();
	numerote s1 = genN();
	numerote s2 = genN();
	numerote s3 = genN();
	numerote s4 = genN();
	numerote s5 = genN();
	
	exp2N(e224,224);
	exp2N(e96,96);
	exp2N(e0,0);
	resN(p224,e224,e96);
	sumN(p224,p224,e0);
	
	printf("p224:=%s;\n",txtN(p224));
	
	s1[0] = C[0];		//0,1
	s1[1] = C[1];		//2,3
	s1[2] = C[2];		//4,5
	s1[3] = C[3] & ~mskM;	//6
		
	s2[0] = 0;
	s2[1] = C[3] & mskM;	//-,7
	s2[2] = C[4];		//8,9
	s2[3] = C[5] & ~mskM;	//10,-

	s3[0] = 0;
	s3[1] = C[5] & mskM;	//-,11
	s3[2] = C[6];		//12,13
	s3[3] = 0;
	
	s4[0] = (C[3] >> t_mrq) ^ (C[4] << t_mrq);
	s4[1] = (C[4] >> t_mrq) ^ (C[5] << t_mrq);
	s4[2] = (C[5] >> t_mrq) ^ (C[6] << t_mrq);
	s4[3] = (C[6] >> t_mrq);

	s5[0] = (C[5] >> t_mrq) ^ (C[6] << t_mrq);
	s5[1] = (C[6] >> t_mrq);
	s5[2] = 0;
	s5[3] = 0;

	sumN(R,s1,s2);
	if(cmpN(R,p224)>=0)
		{
		resN(R,R,p224);
		}
	sumN(R,R,s3);
	if(cmpN(R,p224)>=0)
		{
		resN(R,R,p224);
		}
	resN(R,R,s4);
	resN(R,R,s5);
	return(0);
	}
/***********************************************/

/***********************************************/
numero p256(numerote R, numerote C)
	{
	numerote p256 = genN();
	numerote e256 = genN();
	numerote e224 = genN();
	numerote e192 = genN();
	numerote e96 = genN();
	numerote e0 = genN();
	numerote s1 = genN();
	numerote s2 = genN();
	numerote s3 = genN();
	numerote s4 = genN();
	numerote s5 = genN();
	numerote s6 = genN();
	numerote s7 = genN();
	numerote s8 = genN();
	numerote s9 = genN();
	
	exp2N(e256,256);
	exp2N(e224,224);
	exp2N(e192,192);
	exp2N(e96,96);
	exp2N(e0,0);
	resN(p256,e256,e224);
	sumN(p256,p256,e192);
	sumN(p256,p256,e96);
	resN(p256,p256,e0);
	
	printf("p256:=%s;\n",txtN(p256));
	
	s1[0] = C[0];		//0,1
	s1[1] = C[1];		//2,3
	s1[2] = C[2];		//4,5
	s1[3] = C[3];		//6,7
		
	s2[0] = 0;
	s2[1] = C[5] & mskM;	//-,11
	s2[2] = C[6];		//12,13
	s2[3] = C[7];		//14,15

	s3[0] = 0;
	s3[1] = C[6] << t_mrq;	//-,12
	s3[2] = (C[6] >> t_mrq) ^ (C[7] << t_mrq);
	s3[3] = C[7] >> t_mrq;	//15,0;

	s4[0] = C[4];
	s4[1] = C[5] & ~mskM;
	s4[2] = 0;
	s4[3] = C[7];

	s5[0] = (C[4] >> t_mrq) ^ (C[5] << t_mrq);
	s5[1] = (C[5] >> t_mrq) ^ (C[6] & mskM);
	s5[2] = C[7];
	s5[3] = (C[6] >> t_mrq) ^ (C[4] << t_mrq);;

	s6[0] = (C[5] >> t_mrq) ^ (C[6] << t_mrq);
	s6[1] = (C[6] >> t_mrq);
	s6[2] = 0;
	s6[3] = (C[4] & ~mskM) ^ (C[5] << t_mrq);

	s7[0] = C[6];
	s7[1] = C[7];
	s7[2] = 0;
	s7[3] = (C[4] >> t_mrq) ^ (C[5] & mskM);

	s8[0] = (C[6] >> t_mrq) ^ (C[7] << t_mrq);
	s8[1] = (C[7] >> t_mrq) ^ (C[4] << t_mrq);
	s8[2] = (C[4] >> t_mrq) ^ (C[5] << t_mrq);
	s8[3] = (C[6] & mskM);

	s9[0] = C[7];
	s9[1] = (C[4] & mskM);
	s9[2] = C[5];
	s9[3] = (C[6] & mskM);

	sumN(R,s1,s2);
	if(cmpN(R,p256)>=0)
		{resN(R,R,p256);}
	sumN(R,R,s2);
	if(cmpN(R,p256)>=0)
		{resN(R,R,p256);}
	sumN(R,R,s3);
	if(cmpN(R,p256)>=0)
		{resN(R,R,p256);}
	sumN(R,R,s3);
	if(cmpN(R,p256)>=0)
		{resN(R,R,p256);}
	sumN(R,R,s4);
	if(cmpN(R,p256)>=0)
		{resN(R,R,p256);}
	sumN(R,R,s5);
	if(cmpN(R,p256)>=0)
		{resN(R,R,p256);}
	resN(R,R,s6);
	resN(R,R,s7);
	resN(R,R,s8);
	resN(R,R,s9);
	return(0);
	}
/***********************************************/


/***********************************************/
numero p384(numerote R, numerote C)
	{
	numerote p384 = genN();
	numerote e384 = genN();
	numerote e128 = genN();
	numerote e96 = genN();
	numerote e32 = genN();
	numerote e0 = genN();
	numerote s1 = genN();
	numerote s2 = genN();
	numerote s3 = genN();
	numerote s4 = genN();
	numerote s5 = genN();
	numerote s6 = genN();
	numerote s7 = genN();
	numerote s8 = genN();
	numerote s9 = genN();
	numerote s10 = genN();
	
	exp2N(e384,384);
	exp2N(e128,128);
	exp2N(e96,96);
	exp2N(e32,32);
	exp2N(e0,0);
	resN(p384,e384,e128);
	resN(p384,p384,e96);
	sumN(p384,p384,e32);
	resN(p384,p384,e0);
	
	printf("p384:=%s;\n",txtN(p384));
	
	s1[0] = C[0];		//0,1
	s1[1] = C[1];		//2,3
	s1[2] = C[2];		//4,5
	s1[3] = C[3];		//6,7
	s1[4] = C[4];		//8,9
	s1[5] = C[5];		//10,11

	s2[0] = 0;
	s2[1] = 0;
	s2[2] = (C[10] >> t_mrq) ^ (C[11] << t_mrq);
	s2[3] = (C[11] >> t_mrq);
	s2[4] = 0;
	s2[5] = 0;

	s3[0] = C[6];
	s3[1] = C[7];
	s3[2] = C[8];
	s3[3] = C[9];
	s3[4] = C[10];
	s3[5] = C[11];

	s4[0] = (C[10] >> t_mrq) ^ (C[11] << t_mrq);
	s4[1] = (C[11] >> t_mrq) ^ (C[6] << t_mrq);
	s4[2] = (C[6] >> t_mrq) ^ (C[7] << t_mrq);
	s4[3] = (C[7] >> t_mrq) ^ (C[8] << t_mrq);
	s4[4] = (C[8] >> t_mrq) ^ (C[9] << t_mrq);
	s4[5] = (C[9] >> t_mrq) ^ (C[10] << t_mrq);

	s5[0] = (C[11] & mskM);
	s5[1] = (C[10] << t_mrq);
	s5[2] = C[6];
	s5[3] = C[7];
	s5[4] = C[8];
	s5[5] = C[9];

	s6[0] = 0;
	s6[1] = 0;
	s6[2] = C[10];
	s6[3] = C[11];
	s6[4] = 0;
	s6[5] = 0;

	s7[0] = (C[10] & ~mskM);
	s7[1] = (C[10] & mskM);
	s7[2] = C[11];
	s7[3] = 0;
	s7[4] = 0;
	s7[5] = 0;

	s8[0] = (C[11] >> t_mrq) ^ (C[6] << t_mrq);
	s8[1] = (C[6] >> t_mrq) ^ (C[7] << t_mrq);
	s8[2] = (C[7] >> t_mrq) ^ (C[8] << t_mrq);
	s8[3] = (C[8] >> t_mrq) ^ (C[9] << t_mrq);
	s8[4] = (C[9] >> t_mrq) ^ (C[10] << t_mrq);
	s8[5] = (C[10] >> t_mrq) ^ (C[11] << t_mrq);

	s9[0] = (C[10] << t_mrq);
	s9[1] = (C[10] >> t_mrq) ^ (C[11] << t_mrq);
	s9[2] = (C[11] << t_mrq);
	s9[3] = 0;
	s9[4] = 0;
	s9[5] = 0;

	s10[0] = 0;
	s10[1] = (C[11] & mskM);
	s10[2] = (C[11] >> t_mrq);
	s10[3] = 0;
	s10[4] = 0;
	s10[5] = 0;

	sumN(R,s1,s2);
	if(cmpN(R,p384)>=0)
		{resN(R,R,p384);}
	sumN(R,R,s2);
	if(cmpN(R,p384)>=0)
		{resN(R,R,p384);}
	sumN(R,R,s3);
	if(cmpN(R,p384)>=0)
		{resN(R,R,p384);}
	sumN(R,R,s4);
	if(cmpN(R,p384)>=0)
		{resN(R,R,p384);}
	sumN(R,R,s5);
	if(cmpN(R,p384)>=0)
		{resN(R,R,p384);}
	sumN(R,R,s6);
	if(cmpN(R,p384)>=0)
		{resN(R,R,p384);}
	sumN(R,R,s7);
	if(cmpN(R,p384)>=0)
		{resN(R,R,p384);}
	resN(R,R,s8);
	resN(R,R,s9);
	resN(R,R,s10);
	return(0);
	}
/***********************************************/


/***********************************************/
numero p521(numerote R, numerote C)
	{
	numerote p521 = genN();
	numerote e521 = genN();
	numerote e0 = genN();
	numerote s1 = genN();
	numerote s2 = genN();
	numero i;
		
	exp2N(e521,521);
	exp2N(e0,0);
	resN(p521,e521,e0);
	
	printf("p521:=%s;\n",txtN(p521));
	
	for(i=0; i<8; i++)
		{
		s1[i] = C[i];
		}
	s1[8] = C[8] & 0x01FF;
	
	for(i=0; i<8; i++)
		{
		s2[i] = C[i+8];
		}
		
	for(i=0; i<9; i++)
		{
		div2N(s2,s2);
		}

	sumN(R,s1,s2);
	if(cmpN(R,p521)>=0)
		{resN(R,R,p521);}
	return(0);
	}
/***********************************************/
