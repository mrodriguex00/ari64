#include"division.h"
	
/**************************************************/
numero divPN(numerote N, numerote A)
	{
	numero i = t_pal-2;
	do
		{
		N[i] = A[i+1];
		}
	while((i--)>0);
	
	return(0);
	}
/**************************************************/


/*** DIVISION CON REDUCCION SIN RESTAURACION ******/
numero divN(numerote Q, numerote R, numerote A, numerote B)
	{
	numerote Ri = genN();
	numerote Ri_ = genN();
	numerote W = genN();
	numerote N = genN();
	numerote CERO = genN();
	numerote div = genN();
	numero i;
	numero k= log2N(A) + 1;
	numero l= log2N(B) + 1;
	
	obtN(CERO,"00");
	obtN(div,"00");
	asgN(Ri_,A);		
	asgN(N,B);
	
	for(i=l; i<k; i++)
		{
		mul2N(N,N);	//Desplazamiento de bit
		}
		
	for(i=l;i<=k;i++)
		{
		if(cmpN(Ri_,CERO)<0)
			{
			sumN(Ri,Ri_,N);
			exp2N(W,(k-i));//Desplazamiento de bit
			resN(div,div,W);
			}
		else
			{
			resN(Ri,Ri_,N);
			exp2N(W,(k-i));//Desplazamiento de bit
			sumN(div,div,W);
			}
		
		asgN(Ri_,Ri);
		div2N(N,N);		//Desplazamiento de bit
		}
		
	if(cmpN(Ri_,CERO)<0)
		{
		sumN(Ri,Ri_,B);
		exp2N(W,0x00);		//Desplazamiento de bit
		resN(div,div,W);
		}
	
	asgN(R,Ri);
	asgN(Q,div);
		
	free(Ri);
	free(Ri_);
	free(W);
	free(CERO);
	return(0);
	}
/**************************************************/
