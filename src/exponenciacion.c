#include"exponenciacion.h"


/*** EXPONENCIACION BNARIA DERECHA A IZQUIERDA ****/
numero ediN(numerote E, numerote G, numero e)
	{
	numerote A = genN();
	numerote S = genN();
	
	asgN(S,G);
	obtN(A,"01");

	while(e!=0)
		{
		if(e & 0x01 == 1)
			{
			mulN(A,A,S);
			}
		e = e >> 1;
		if(e != 0)
			{
			mulN(S,S,S);			
			}
		}
	asgN(E,A);	
	free(S);	
	free(A);
	return(0);
	}
/**************************************************/


/*** EXPONENCIACION BNARIA IZQUIERDA A DERECHA ****/
numero eidN(numerote E, numerote G, numero e)
	{
	numerote A = genN();
	numerote S = genN();
	numero i;
	numero n = logB2(e);
		
	asgN(S,G);
	obtN(A,"01");
	
	while(n-- > 0)
		{
		mulN(A,A,A);
		if((e & (0x01 << n)) != 0)
			{
			mulN(A,A,G);
			}
		}
	asgN(E,A);	
	free(S);	
	free(A);
	return(0);
	}
/**************************************************/

