#include"numerotes.h"
/**************************************************/

	
/**************************************************/
numerote genN()
	{
	numerote N = calloc(t_pal,sizeof(numero));
	if(N == NULL)
		{fprintf(stderr,"problema en la memoria");}
	return(N);
	}
/**************************************************/

	
/**************************************************/
numero azrN(numerote N, numero k)
	{
	numero i;
	numero n=(k/t_arq)+1;
	for(i=0; i<n; i++)
		{
		N[i] = ((numero)rand() << t_mrq) ^ rand();
		}
	return(0);
	}
/**************************************************/

	
/**************************************************/
numero obtN(numerote N, texto Sx)
	{
	char S[t_txt];
	char vS[t_txt];
	numero i;
	numero s;
	char signo = Sx[0] == '-' ? 'F' : '0';
		
	lmpS(S,signo,t_txt);
	strcpy(S,Sx);
	revS(S);
	s=strlen(S);
	S[s]=signo;
	S[t_txt]=0;

	for(i=0; i<t_pal; i++)
		{
		strncpy(vS,S+(i*t_car),t_car);
		vS[t_car]=0;
		revS(vS);
		sscanf(vS,formatoEnt,&N[i]);
		}
	return(0);
	}
/**************************************************/

	
/**************************************************/
texto txtN(numerote N)
	{
	numero i;
	texto txt = malloc(sizeof(char)*t_txt);
	texto cad;
	numerote C = genN();
	numerote Cm = genN();
	numerote uno = genN();
	
	if((N[t_pal-1] & mskP) == 0)
		{
		cad = cadN(N);
		sprintf(txt,"+0x%s",cad);
		}
	else
		{
		inv2N(C,N);
		obtN(uno,"01");
		sumN(Cm,C,uno);
		cad = cadN(Cm);
		sprintf(txt,"-0x%s",cad);
		}
	
	free(C);
	free(Cm);
	free(uno);
	free(cad);
	return(txt);
	}
/**************************************************/

	
/**************************************************/
texto cadN(numerote N)
	{
	texto cad = malloc(sizeof(char)*t_txt);
	numero i;
	numero b=0;
	
	for(i=0; i<t_pal; i++)
		{
		if(N[t_pal-i-1] != 0 && b==0 || i==t_pal-1)
			{b=1;}
		if(b==1){ 
			sprintf(cad,formatoSal,cad,N[t_pal-i-1]);
			}
		}
	return(cad);
	}
/**************************************************/

	
/**************************************************/
numero sumN(numerote N,numerote A,numerote B)
	{
	numero i;
	numero crrA = 0;
	numero crrB = 0;
	numero carr = 0;
	numero desb = 0;
	numerote suma = genN();
		
	for(i=0; i<t_pal; i++)
		{
		desb = (A[i] + B[i] + carr); 
		suma[i] = desb & mskF;
		crrB = ( (A[i] & ~mskM) + (B[i] & ~mskM) + carr) >> t_mrq;
		crrA = crrB + (A[i]>>t_mrq) + (B[i]>>t_mrq);
		carr = crrA >> t_mrq;
		}
	asgN(N,suma);
//	free(suma);		
	return(0);
	}
/**************************************************/

	
/**************************************************/
numero resN(numerote N, numerote A,numerote B)
	{
	numerote tmp1 = genN();
	numerote tmp2 = genN();
	numerote uno1 = genN();
	
	obtN(uno1,"01");
	
	inv2N(tmp1,B);
	sumN(tmp2,tmp1,uno1);
	sumN(N,A,tmp2);
	
//	free(tmp1);
//	free(tmp2);
	free(uno1);		
	return(0);
	}
/**************************************************/

	
/**************************************************/
numero asgN(numerote N, numerote O)
	{	
	numero i;
	
	for(i=0; i<t_pal; i++)
		{
		N[i] = O[i];
		}

	return(0);
	}
/**************************************************/

	
/**************************************************/
numero inv2N(numerote N, numerote O)
	{	
	numero i;
	numerote div = genN();

	for(i=0; i<t_pal; i++)
		{
		N[i] = ~O[i];
		}

	return(0);
	}
/**************************************************/

	
/**************************************************/
int cmpN(numerote A,numerote B)
	{
	numero i = t_pal-1;
	numero sA = A[i] & mskP;
	numero sB = B[i] & mskP;
	numero uA = A[i] & ~mskP;
	numero uB = B[i] & ~mskP;
	
	if(sA<sB){return(1);}
	if(sA>sB){return(-1);}
	
	for(i=0; i<t_pal; i++)
		{
		if(A[t_pal-i-1] > B[t_pal-i-1])
			{
			return(1);
			}
		if(A[t_pal-i-1] < B[t_pal-i-1])
			{
			return(-1);
			}
		}
	return(0);
	}
/**************************************************/

	
/**************************************************/
numero parN(numerote N)
	{
	numero paridad = (N[0]) & msk1;
	return(paridad);
	}
/**************************************************/

	
/**************************************************/
numero div2N(numerote N, numerote A)
	{
	numero i = t_pal-1;
	numero carr = A[i] & mskP;
	numerote div = genN();
		
	do
		{
		div[i] = (A[i] >> 1) ^ carr;
		carr = A[i] << (t_arq-1);
		}
	while((i--)>0);
	
	asgN(N,div);
	free(div);
	return(0);
	}
/**************************************************/

	
/**************************************************/
numero mul2N(numerote N, numerote A)
	{
	numero i = 0;
	numero carr = 0;
	numerote mul = genN();
		
	do
		{	
		mul[i] = (A[i] << 1) ^ carr;
		carr = A[i] >> (t_arq-1);
		}
	while((i++) < t_pal-1);
	
	asgN(N,mul);
	free(mul);
	return(0);
	}
/**************************************************/

	
/*** POTENCIA DE 2 ********************************/
numero exp2N(numerote N, numero e)
	{
	numero i;
	numero e_max = t_arq*t_pal;
	obtN(N,"01");
	
	for(i=0;i<e && i<e_max;i++)
		{
		mul2N(N,N);	//Desplazamiento de bit
		}
	return(0);
	}
/**************************************************/


/*** LOGARITMO BASE 2 *****************************/
numero log2N(numerote A)
	{
	numero i = 0;
	numerote W = genN();
	numerote UNO = genN();
	
	obtN(UNO,"01");
	asgN(W,A);
	do
		{
		div2N(W,W);	//Desplazamiento de bit
		i++;
		}
	while(cmpN(W,UNO)>0);
	
	free(W);
	free(UNO);
	return(i);
	}
/**************************************************/

