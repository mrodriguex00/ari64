#include"multiplicacion.h"

	
/*** MULTIPLICACION CLASICA ***********************/
numero mulEN(numerote N, numerote A, numerote B)
	{
	numero i,j;
	numero t_lim = ((t_pal>>1)+1);
	numero crrA = 0;
	numero crrB = 0;
	desbor carr = 0;
	desbor desb = 0;
	numerote mul = genN();
		
	for(i=0; i<t_lim; i++)
		{
		carr=0;
		for(j=0; j<t_lim; j++)
			{
			desb = mul[i+j] + (desbor)((desbor)A[i] * (desbor)B[j] + (desbor)carr);
			mul[i+j] = (numero)(desb);
			carr = (numero)(desb >> (t_arq));
			}
		}
	
	asgN(N,mul);
	free(mul);
	return(0);
	}
/**************************************************/


/*** MULTIPLICACION BNARIA IZQUIERDA A DERECHA ****/
numero mulN(numerote E, numerote K, numerote P_)
	{
	numerote Q = genN();
	numerote P = genN();
	numero i;
	numero j;
	
	asgN(P,P_);
	obtN(Q,"00");

	for(i=0; i<t_pal; i++)
		{
		for(j=0; j<t_arq; j++)
			{
			if((K[i] >> j) & 0x01 == 1)
				{
				sumN(Q,Q,P);
				}
			mul2N(P,P);	//Desplazamiento de bit
			}
		}
	asgN(E,Q);
	free(Q);
	return(0);
	}
/**************************************************/


/*** MULTIPLICACION MONTGOMERY ********************/
numero mulMont2(numerote u, numerote x_, numerote y_, numerote r, numerote n, numerote r_1, numerote n_)
	{
	numerote red = genN();
	numerote t = genN();
	numerote m = genN();
	numerote U = genN();
	numerote CERO = genN();
	obtN(CERO,"00");
	int i;
	
	mulN(t,x_,y_);
	printf("r:=%s;\n",txtN(r));
//	mulN(t,x_,r);
//	mulN(m,y_,r);
	printf("x_:=%s;\n",txtN(x_));
	printf("y_:=%s;\n",txtN(y_));
	printf("t:=%s;\n",txtN(t));
//	mulN(t,t,m);
//	divN(m,t,r);
	mulN(m,t,r_1);
	printf("m:=%s;\n",txtN(m));
	rbrN(m,m,n,U);
	
	if(cmpN(m,n)>=0)
		{
		resN(m,m,n);
		}
	asgN(u,m);
	return(0);
	}
/**************************************************/


/*** MULTIPLICACION MONTGOMERY ********************/
numero mulMont(numerote u, numerote x_, numerote y_, numerote r, numerote n, numerote r_1, numerote n_)
	{
	numerote red = genN();
	numerote t = genN();
	numerote m = genN();
	numerote U = genN();
	numerote q = genN();
	numero k;
	
	k = log2N(n) + 1;
	bruN(U,n,k);
	
	mulN(t,x_,y_);
	mulN(m,t,r_1);
	rbrN(m,m,n,U);
	
	if(cmpN(m,n)>=0)
		{
		resN(m,m,n);
		}
	asgN(u,m);
	return(0);
	}
/**************************************************/



/*** CONFIGURAR ENTEROS PARA MULTMONT *************/
numero confMN(numerote Y_, numerote Y, numerote R, numerote N)
	{
	numerote U = genN();
	numero k;
	
	k = log2N(N) + 1;
	bruN(U,N,k);
	
	mulN(Y_,Y,R);
	rbrN(Y_,Y_,N,U);
	return(0);
	}
/**************************************************/
/*
	numerote U = genN();
	numero exp2 = log2N(R);
	printf("exp2:=%li;\n",exp2);
	while(exp2-- > 0)
		{
		mul2N(Y,Y);
	printf("Y:=%s;\n",txtN(Y));
		}
	rbrN(Y_,Y_,N,U);
*/
numero redMont(numerote u, numerote y, numerote r, numerote n)
	{
	numerote red = genN();
	numerote t = genN();
	numerote y_ = genN();
	numerote n_1 = genN();
	numerote m = genN();
	numerote U = genN();
	numerote x = genN();
	numerote CERO = genN();

	obtN(CERO,"00");
	
	resN(y_,CERO,y);
	invPN(n_1,n,r);
	mulN(u,n_1,y_);
	rbrN(u,u,r,U);
	
	sumN(x,y,u);
	divN(x,x,r);
		
	if(cmpN(x,n)>0)
		{
		resN(x,x,n);
		}
	asgN(u,x);
	return(0);
	}


