#include"reduccion.h"

/*** REDUCCION CON RESTAURACIÓN *******************/
numero rcrN(numerote R, numerote A, numerote B)
	{
	numerote Ri = genN();
	numerote Ri_ = genN();
	numerote W = genN();
	numerote N = genN();
	numerote CERO = genN();
	numero i;
	numero k= log2N(A) + 1;
	numero l= log2N(B) + 1;
	
	obtN(CERO,"00");
	asgN(Ri_,A);
		
	asgN(N,B);
	for(i=l; i<k; i++)
		{
		mul2N(N,N);	//Desplazamiento de bit
		}

	for(i=l;i<=k;i++)
		{
		resN(Ri,Ri_,N);
		if(cmpN(Ri,CERO)<0)
			{
			asgN(Ri,Ri_);
			}
		asgN(Ri_,Ri);
		div2N(N,N);	//Desplazamiento de bit
		}
		
	asgN(R,Ri);	
	free(Ri);
	free(Ri_);
	free(W);
	free(N);
	free(CERO);
	return(0);
	}
/**************************************************/


/*** REDUCCION SIN RESTAURACIÓN *******************/
numero rsrN(numerote R, numerote A, numerote B)
	{
	numerote Ri = genN();
	numerote Ri_ = genN();
	numerote W = genN();
	numerote N = genN();
	numerote CERO = genN();
	numero i;
	numero k= log2N(A) + 1;
	numero l= log2N(B) + 1;
	
	obtN(CERO,"00");
	asgN(Ri_,A);		
	asgN(N,B);
	
	for(i=l; i<k; i++)
		{
		mul2N(N,N);	//Desplazamiento de bit
		}
		
	for(i=l;i<=k;i++)
		{
		if(cmpN(Ri_,CERO)<0)
			{
			sumN(Ri,Ri_,N);
			}
		else
			{
			resN(Ri,Ri_,N);
			}
		
		asgN(Ri_,Ri);
		div2N(N,N);	//Desplazamiento de bit
		}
		
	if(cmpN(Ri_,CERO)<0)
		{
		sumN(Ri,Ri_,B);
		}
	
	asgN(R,Ri);
	free(Ri);
	free(Ri_);
	free(W);
	free(N);
	free(CERO);
	return(0);
	}
/**************************************************/


/*** REDUCCION DE BARRET **************************/
numero rbrN(numerote R, numerote X, numerote P, numerote U)
	{
	numerote bk1 = genN();
	numerote R1 = genN();
	numerote R2 = genN();
	numerote Q1 = genN();
	numerote Q2 = genN();
	numerote Q3 = genN();
	numerote CERO = genN();
	numero k= log2N(P) + 1;
	numero i;
	
	obtN(CERO,"00");

	exp2N(bk1,k+1);		//Desplazamiento de bit
	asgN(Q1,X);
	
	for(i=0;i<k-1;i++)
		{
		div2N(Q1,Q1);	//Desplazamiento de bit
		}

	mulN(Q2,Q1,U);
	asgN(Q3,Q2);
	
	for(i=0;i<k+1;i++)
		{
		div2N(Q3,Q3);	//Desplazamiento de bit
		}

	asgN(R1,X);
	mulN(R2,Q3,P);	
	resN(R,R1,R2);
	
	if(cmpN(R,CERO)<0)
		{
		sumN(R,R,bk1);
		}

	while(cmpN(R,P)>0)
		{
		resN(R,R,P);
		}
	
	free(bk1);
	free(R1);
	free(R2);
	free(Q1);
	free(Q2);
	free(Q3);
	free(CERO);
	return(0);
	}
/**************************************************/


/*** PRECOMPUTO DE U PARA REDUCCION DE BARRET *****/
numero bruN(numerote U, numerote P, numero k)
	{
	numerote b2k = genN();
	numerote R = genN();
	exp2N(b2k,2*k);		//Desplazamiento de bit
	divN(U,R,b2k,P);

	free(R);
	return(0);
	}
/**************************************************/


/*** REDUCCION DE BARRET NO OPTIMIZADA ************/
numero rbrN2(numerote R, numerote X, numerote P, numerote U)
	{
	numerote bk1 = genN();
	numerote bk_1 = genN();
	numerote R1 = genN();
	numerote R2 = genN();
	numerote Q1 = genN();
	numerote Q2 = genN();
	numerote Q3 = genN();
	numerote CERO = genN();
	numero k= log2N(P) + 1;
	
	obtN(CERO,"00");
	
	bruN(U,P,k);

	exp2N(bk_1,k-1);
	exp2N(bk1,k+1);
	divN(Q1,R,X,bk_1);
	mulN(Q2,Q1,U);
	divN(Q3,R,Q2,bk1);
	
	asgN(R1,X);
	mulN(R2,Q3,P);
	
	resN(R,R1,R2);
	
	if(cmpN(R,CERO)<0)
		{
		sumN(R,R,bk1);
		}
	
	while(cmpN(R,P)>=0)
		{
		resN(R,R,P);
		}
	
	free(bk1);
	free(R1);
	free(R2);
	free(Q1);
	free(Q2);
	free(Q3);
	free(CERO);
	return(0);
	}
/**************************************************/
