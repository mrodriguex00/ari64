#include"euclides.h"

mcdX genMCD()
	{
	mcdX N = malloc(sizeof(mcdX));	
	N->g = genN();
	N->d = genN();
	N->s = genN();
	N->t = genN();
	if(N == NULL)
		{puts("problema en la memoria");}
	return(N);
	}
	
numero xmcd(mcdX unMCD, numerote x, numerote y)
	{
	numerote xN = genN(); 
	numerote yN = genN();
	numerote gN = genN();
	numerote tmp1N = genN();
	numerote tmp2N = genN();
	numerote uN = genN();
	numerote vN = genN();
	numerote AN = genN();
	numerote BN = genN();
	numerote CN = genN();
	numerote DN = genN();
	numerote dosN = genN();
	numerote unoN = genN();
	numerote ceroN = genN();

	obtN(gN,"01");
	obtN(ceroN,"00");
	obtN(unoN,"01");
	obtN(tmp1N,"00");
	obtN(tmp2N,"00");
	
	asgN(xN,x);
	asgN(yN,y);
	
	if(cmpN(xN,yN)<0)
		{
		asgN(tmp1N,xN);
		asgN(xN,yN);
		asgN(yN,tmp1N);
		}
	
	while(parN(xN) == 0 && parN(yN) == 0)
		{
		div2N(tmp1N,xN);
		asgN(xN,tmp1N);
		div2N(tmp2N,yN);
		asgN(yN,tmp2N);
		mul2N(tmp1N,gN);
		asgN(gN,tmp1N);
		}

	asgN(uN,xN);
	asgN(vN,yN);
	obtN(AN,"01");
	obtN(BN,"00");
	obtN(CN,"00");
	obtN(DN,"01");

	do
		{
		while(parN(uN) == 0)
			{
			div2N(tmp1N,uN);
			asgN(uN,tmp1N);

			if(parN(AN) == 0 && parN(BN) == 0)
				{
				div2N(tmp1N,AN);
				asgN(AN,tmp1N);
				div2N(tmp2N,BN);
				asgN(BN,tmp2N);
				}
			else
				{
				sumN(tmp1N,AN,yN);
				div2N(AN,tmp1N);
				resN(tmp2N,BN,xN);
				div2N(BN,tmp2N);
				}
			}
		
		while(parN(vN) == 0)
			{
			div2N(tmp1N,vN);
			asgN(vN,tmp1N);

			if(parN(CN) == 0 && parN(DN) == 0)
				{
				div2N(tmp1N,CN);
				asgN(CN,tmp1N);
				div2N(tmp2N,DN);
				asgN(DN,tmp2N);
				}
			else
				{
				sumN(tmp1N,CN,yN);
				div2N(CN,tmp1N);
				resN(tmp2N,DN,xN);
				div2N(DN,tmp2N);
				}	
			}

		if(cmpN(uN,vN) >= 0)
			{
			resN(tmp1N,uN,vN);
			asgN(uN,tmp1N);
			resN(tmp1N,AN,CN);
			asgN(AN,tmp1N);
			resN(tmp1N,BN,DN);
			asgN(BN,tmp1N);
			}
		else
			{
			resN(tmp1N,vN,uN);
			asgN(vN,tmp1N);
			resN(tmp1N,CN,AN);
			asgN(CN,tmp1N);
			resN(tmp1N,DN,BN);
			asgN(DN,tmp1N);
			}
		}
	while(cmpN(uN,ceroN) != 0);
		
	asgN(unMCD->d,vN);
	asgN(unMCD->s,CN);
	asgN(unMCD->t,DN);
	asgN(unMCD->g,gN);

	return(0);
	}
/**************************************************/


/*** INVERSO MULTIPLICATIVO EN Fp *****************/
numero invPN(numerote U, numerote A, numerote P)
	{
	numerote xN = genN(); 
	numerote yN = genN();
	numerote uN = genN();
	numerote vN = genN();
	numerote AN = genN();
	numerote CN = genN();
	numerote UNO = genN();
	numerote CERO = genN();
	numerote tmp1N = genN();

	obtN(UNO,"01");
	obtN(CERO,"00");

	asgN(xN,A);
	asgN(yN,P);

	asgN(uN,xN);
	asgN(vN,yN);
	obtN(AN,"01");
	obtN(CN,"00");

	do
		{
		while(parN(uN) == 0)
			{
			div2N(uN,uN);

			if(parN(AN) == 0)
				{
				div2N(AN,AN);
				}
			else
				{
				sumN(AN,AN,yN);
				div2N(AN,AN);
				}
			}
		
		while(parN(vN) == 0)
			{
			div2N(vN,vN);

			if(parN(CN) == 0)
				{
				div2N(CN,CN);
				}
			else
				{
				sumN(CN,CN,yN);
				div2N(CN,CN);
				}	
			}

		if(cmpN(uN,vN) >= 0)
			{
			resN(uN,uN,vN);
			resN(AN,AN,CN);
			}
		else
			{
			resN(vN,vN,uN);
			resN(CN,CN,AN);
			}
		}
	while(cmpN(uN,CERO) != 0);
	
	if(cmpN(uN,UNO) == 0)
			{
			asgN(U,AN);
			}
		else
			{
			asgN(U,CN);
			}
	}
/**************************************************/


/*** INVERSO MULTIPLICATIVO EN Fp *****************/
numero invPN2(numerote U, numerote A, numerote P)
	{
	mcdX unMCD = genMCD();
	
//	rsrN(A,A,P);
	xmcd(unMCD, A, P);
	asgN(U,unMCD->t);
	return(0);
	}
/**************************************************/
