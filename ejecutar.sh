#!/bin/sh
echo "Limpiando..."
rm obj/*.o
rm bin/tarea2
echo "Ok"
echo "Construyendo Makefile..."
./makeMakefile.sh -c ./src/ -i ./lib/ -a ./app/tarea2.c -o ./obj -b bin/tarea2
echo "Ok"
echo "Compilando..."
make
echo "Ok"
echo "Ejecutando..."
./bin/tarea2 > salida.magma
echo "Listo, consulte archivo salida.magma"
