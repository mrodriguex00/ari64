#include<time.h>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<inttypes.h>

#define t_pal 80
#define t_arq 64
#define t_mrq t_arq/2
#define t_car t_arq/4
#define t_txt 2*t_pal*t_car
#define mskM 0xFFFFFFFF00000000
#define mskF 0xFFFFFFFFFFFFFFFF
#define mskP 0x8000000000000000
#define msk1 0x0000000000000001
#define formatoEnt "%016lX"
#define formatoSal "%s%016lX"

typedef __int128 desbor;
typedef uint64_t numero;
typedef numero * numerote;
typedef char * texto;

numerote genN();
numero azrN(numerote N, numero k);
numero obtN(numerote N, texto Sx);
texto txtN(numerote N);
texto cadN(numerote N);
numero sumN(numerote N, numerote A,numerote B);
numero resN(numerote N, numerote A,numerote B);
numero asgN(numerote D, numerote O);
numero inv2N(numerote N, numerote O);
int cmpN(numerote A, numerote B);
numero parN(numerote A);
numero div2N(numerote N, numerote A);
numero log2N(numerote A);

