#include<stdio.h>
#include"numerotes.h"
#include"multiplicacion.h"
#include"division.h"
#include"exponenciacion.h"
#include"reduccion.h"
#include"euclides.h"
#include"nist.h"

int main()
	{
	numerote x = genN();
	numerote y = genN();
	numerote w = genN();
	numerote u = genN();
	numerote res = genN();
	numerote sum = genN();
	numerote mul = genN();
	numerote red = genN();
	numerote P = genN();
	numerote U = genN();
	numerote exp = genN();
	numerote UNO = genN();
	numerote DOS = genN();
	numerote CERO = genN();
	///////////////////////////////////////////////
	numerote x_ = genN();
	numerote y_ = genN();
	numerote r = genN();	//base de montgomery
	numerote n = genN();	//primo de montgomery
	numerote r_1 = genN();	//base de montgomery
	numerote n_ = genN();	//primo de montgomery
	numerote m_ = genN();	//log de P de mont
	numero log = 0;		//log de P de mont
	numero k;
	numero e;
	mcdX unMCD = genMCD();
	texto str1[512];
	texto str2[512];
	
	obtN(CERO,"00");
	obtN(UNO,"01");
	obtN(DOS,"02");
	srand(time(NULL));

	azrN(x,1024);
	azrN(y,1024);
	
	//Primo (2^1024) + (2^9) + (2^7) + (2^1) + 1 de 1024 bits
	obtN(P,"01");
	exp2N(exp,1024);
	resN(P,exp,P);
	exp2N(exp,7);
	resN(P,P,exp);
	exp2N(exp,4);
	sumN(P,P,exp);
	exp2N(exp,3);
	sumN(P,P,exp);

	puts("/*** 0 <= x,y <= 2^2048 ****************************/");
	printf("x   :=%s;\n",txtN(x));
	printf("y   :=%s;\n",txtN(y));
	puts("/***************************************************/");
	puts(" ");
	puts("/*** P = 2^1024 - 2^7 + 2^4 + 2^3 - 1 **************/");
	printf("P   :=%s;\n",txtN(P));
	puts("/***************************************************/");
	puts(" ");
	

	puts("/*** Adicion ***************************************/");
	sumN(sum,x,y);
	printf("suma:=%s;\n",txtN(sum));

	puts("/*** Comprobación **********************************/");
	puts("print \"\\n suma (x+y):\";");
	puts("(x+y) eq suma;");
	puts("/***************************************************/");
	puts(" ");
	

	puts("/*** Sustraccion ***********************************/");
	resN(res,x,y);
	printf("resta:=%s;\n",txtN(res));

	puts("/*** Comprobación **********************************/");
	puts("print \"\\n resta (x-y):\";");
	puts("(x-y) eq resta;");
	puts("/***************************************************/");
	puts(" ");
	

	puts("/*** Multiplicacion entera *************************/");
	mulN(mul,x,y);
	printf("mul :=%s;\n",txtN(mul));

	puts("/*** Comprobación **********************************/");
	puts("print \"\\n mul: (x*y):\";");
	puts("(x*y) eq mul;");
	puts("/***************************************************/");
	puts(" ");


	puts("/*** Mult modular con restauracion *****************/");
	rcrN(red,mul,P);
	printf("mul:=%s;\n",txtN(red));

	puts("/*** Comprobación **********************************/");
	puts("print \"\\n mul: (x*y) mod P con restauracion:\";");
	puts("((x*y) mod P) eq mul;");
	puts("/***************************************************/");
	puts(" ");
	

	puts("/*** Mult modular sin restauracion *****************/");
	rsrN(red,mul,P);
	printf("mul:=%s;\n",txtN(red));

	puts("/*** Comprobación **********************************/");
	puts("print \"\\n mul: (x*y) mod P sin restauracion:\";");
	puts("((x*y) mod P) eq mul;");
	puts("/***************************************************/");
	puts(" ");


	puts("/*** Mult modular con Barret ***********************/");
	k = log2N(P) + 1;
	bruN(U,P,k);
	rbrN(red,mul,P,U);
	printf("mul:=%s;\n",txtN(red));

	puts("/*** Comprobación **********************************/");
	puts("print \"\\n mul: (x*y) mod P con Barret:\";");
	puts("((x*y) mod P) eq mul;");
	puts("/***************************************************/");
	puts(" ");


	puts("/*** Mult modular con Montgomery *******************/");
	exp2N(r,1024);
	asgN(n,P);
	
	xmcd(unMCD,r,n);
	r_1 = unMCD->s;
	n_ = unMCD->t;
	
	confMN(x_, x, r, n);
	confMN(y_, y, r, n);

	mulMont(mul,x_,y_,r,n,r_1,n_);
	mulMont(mul,mul,UNO,r,n,r_1,n_);
	resN(mul,mul,DOS);

	printf("mul:=%s;\n",txtN(mul));

	puts("/*** Comprobación **********************************/");
	puts("print \"\\n mul: (x*y) mod P con Montgomery:\";");
	puts("((x*y) mod P) eq mul;");
	puts("/***************************************************/");
	puts(" ");
	

	puts("/*** Reduccion rapida NIST p193 ********************/");
	azrN(w,193);
	p192(red,w);
	printf("w   :=%s;\n",txtN(w));
	printf("red :=%s;\n",txtN(red));

	puts("/*** Comprobación **********************************/");
	puts("print \"\\n red: w mod p192:\";");
	puts("(w mod p192) eq red;");
	puts("/***************************************************/");
	puts(" ");
	
	
	puts("/*** Reduccion rapida NIST p224 */");
	azrN(w,224);
	p224(red,w);
	printf("w   :=%s;\n",txtN(w));
	printf("red :=%s;\n",txtN(red));

	puts("/*** Comprobación **********************************/");
	puts("print \"\\n red: w mod p224:\";");
	puts("(w mod p224) eq red;");
	puts("/***************************************************/");
	puts(" ");
	
	
	puts("/*** Reduccion rapida NIST p256 */");
	azrN(w,256);
	p256(red,w);
	printf("w   :=%s;\n",txtN(w));
	printf("red :=%s;\n",txtN(red));

	puts("/*** Comprobación **********************************/");
	puts("print \"\\n red: w mod p256:\";");
	puts("(w mod p256) eq red;");
	puts("/***************************************************/");
	puts(" ");
	
	
	puts("/*** Reduccion rapida NIST p384 */");
	azrN(w,384);
	p384(red,w);
	printf("w   :=%s;\n",txtN(w));
	printf("red :=%s;\n",txtN(red));

	puts("/*** Comprobación **********************************/");
	puts("print \"\\n red: w mod p384:\";");
	puts("(w mod p384) eq red;");
	puts("/***************************************************/");
	puts(" ");
	
	
	puts("/*** Reduccion rapida NIST p521 */");
	azrN(w,521);
	p521(red,w);
	printf("w   :=%s;\n",txtN(w));
	printf("red :=%s;\n",txtN(red));

	puts("/*** Comprobación **********************************/");
	puts("print \"\\n red: w mod p521:\";");
	puts("(w mod p521) eq red;");
	puts("/***************************************************/");
	puts(" ");
	
	
	puts("/*** Exponenciacion izq-der */");
	azrN(u,128);
	e=rand()%16;
	eidN(exp,u,e);
	printf("u   :=%s;\n",txtN(u));
	printf("e   :=0x%lX;\n",e);
	printf("exp :=%s;\n",txtN(exp));

	puts("/*** Comprobación **********************************/");
	puts("print \"\\n exp: u^e izq-der:\";");
	puts("(u^e) eq exp;");
	puts("/***************************************************/");
	puts(" ");
	
	
	puts("/*** Exponenciacion der-izq */");
	ediN(exp,u,e);
	printf("u   :=%s;\n",txtN(u));
	printf("e   :=0x%lX;\n",e);
	printf("exp :=%s;\n",txtN(exp));

	puts("/*** Comprobación **********************************/");
	puts("print \"\\n exp: u^e der-izq:\";");
	puts("(u^e) eq exp;");
	puts("/***************************************************/");
	puts(" ");
	
	puts("/***************************************************/");
//	puts("/***************************************************/");
//	puts("/***************************************************/");
	
//	puts("/*** Comprobaciones adicionales ****/");
//	printf("x :=%s;\n",txtN(x));
//	printf("y :=%s;\n",txtN(y));
//	sumN(w,x,y);	//x+y
//	ediN(exp,w,2);	//(x+y)^2
//	printf("exp1 :=%s;\n",txtN(exp));
//	ediN(w,x,2);	//x^2
//	mulN(u,x,y);	//xy
//	mul2N(u,u);	//2xy
//	sumN(u,w,u);	//x^2+2xy
//	ediN(w,y,2);	//y^2
//	sumN(exp,w,u);	//x^2+2xy+y^2
//	printf("exp2 :=%s;\n",txtN(exp));
//	puts("print \"\\n exp1: (x+y)^2, exp2: x^2 + 2xy + y^2:\";");
//	puts("exp1 eq exp2;");
//	puts("/***********************************/");
//	puts(" ");
	
	return(0);
	}

